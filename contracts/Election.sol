pragma solidity >=0.4.22 <0.7.0;

contract Election {
    
    struct Candidate {
        uint id;
        string name;
        uint countVote;
    }
    
    mapping(address => bool) public voters;
    
    mapping(uint => Candidate) public candidates;
    
    uint public candidatesCount;
    
    event votedEvent (
        uint indexed _candidateId
    );
    
    constructor() public {
        addCandidate("moncef essaoudi");
        addCandidate("adam azzour");
    }
    
    function addCandidate(string memory _name) private {
        candidatesCount++;
        candidates[candidatesCount]= Candidate(candidatesCount, _name, 0);
    }
    
    function vote (uint  _candidateId) public{
        
        require(!voters[msg.sender]);
        require(_candidateId > 0 && _candidateId <= candidatesCount);
        voters[msg.sender] = true;
        candidates[_candidateId].countVote++ ;
        emit votedEvent(_candidateId);
    }
}