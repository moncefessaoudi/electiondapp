




## Step 1. Clone the project
`git clone https://gitlab.com/moncefessaoudi/electiondapp.git`

## Step 2. Install dependencies
```
$ cd electiondapp
$ npm install
```
## Step 3. Start Ganache
Open the Ganache GUI client that you downloaded and installed. This will start your local blockchain instance.


## Step 4. Compile & Deploy Election Smart Contract
`$ truffle migrate --reset`




## Step 5. Run the Front End Application
`$ npm run dev`
Visit this URL in your browser: http://localhost:3000
